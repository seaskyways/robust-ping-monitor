package main

import (
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"os/signal"
	"robust-ping-monitor/pingmonitor"
	"robust-ping-monitor/proto"
)

func main() {
	sock, err := net.Listen("unix", "./monitor.sock")
	if err != nil {
		log.Fatalf("couldn't bind to unix domain socket: %v", err)
	}
	defer sock.Close()
	defer os.Remove("./monitor.sock")

	server := grpc.NewServer()
	monitor.RegisterMonitorServer(server, pingmonitor.NewRPCServer())

	go func() {
		sigCh := make(chan os.Signal, 1)
		signal.Notify(sigCh, os.Interrupt)

		<-sigCh
		server.GracefulStop()
	}()

	// Start the RPC server
	if err := server.Serve(sock); err != nil {
		log.Fatalf("unexpected shutdown: %v", err)
	}
}
