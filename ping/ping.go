package ping

import (
	"bytes"
	"context"
	"encoding/binary"
	"golang.org/x/xerrors"
	"math"
	"math/rand"
	"net"
	"syscall"
	"time"

	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
	"golang.org/x/net/ipv6"
)

type constErr string

func (ce constErr) Error() string { return string(ce) }

const (
	ErrConnIsNil        = constErr("connection is nil")
	ErrNotAnEcho        = constErr("not an echo")
	ErrReplyFromSameID  = constErr("reply from same ID")
	ErrTrackersNotEqual = constErr("trackers not equal")
	ErrIsRunning        = constErr("monitor is running")
)

const (
	timeSliceLength  = 8
	trackerLength    = 8
	protocolICMP     = 1
	protocolIPv6ICMP = 58
)

const Offline = Latency(-1)

type Latency int

var (
	ipv4Proto = map[string]string{"ip": "ip4:icmp", "udp": "udp4"}
	ipv6Proto = map[string]string{"ip": "ip6:ipv6-icmp", "udp": "udp6"}
)

// Pinger represents ICMP packet sender/receiver
type Pinger struct {
	// Interval is the wait time between each packet send. Default is 1s.
	Interval time.Duration

	// Timeout specifies a timeout before ping exits, regardless of how many
	// packets have been received.
	Timeout time.Duration

	// Size of packet being sent
	Size int

	// Tracker: Used to uniquely identify packet when non-priviledged
	Tracker int64

	// Source is the source IP address
	Source string

	ipaddr *net.IPAddr
	addr   string

	ipv4      bool
	id        int
	sequence  int
	network   string
	isRunning bool
}

// NewPinger returns a new Pinger struct pointer
func NewPinger() (*Pinger, error) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	pinger := &Pinger{
		Interval: time.Second,
		Timeout:  time.Second * 100000,
		id:       r.Intn(math.MaxInt16),
		network:  "udp",
		Size:     timeSliceLength,
		Tracker:  r.Int63n(math.MaxInt64),
	}
	pinger.SetPrivileged(true)
	return pinger, nil
}

type packet struct {
	bytes  []byte
	nbytes int
	ttl    int
}

// Packet represents a received and processed ICMP echo packet.
type Packet struct {
	// Rtt is the round-trip time it took to ping.
	Rtt time.Duration

	// IPAddr is the address of the host being pinged.
	IPAddr *net.IPAddr

	// Addr is the string address of the host being pinged.
	Addr string

	// NBytes is the number of bytes in the message.
	Nbytes int

	// Seq is the ICMP sequence number.
	Seq int

	// TTL is the Time To Live on the packet.
	Ttl int
}

// SetIPAddr sets the ip address of the target host.
func (p *Pinger) SetIPAddr(ipaddr *net.IPAddr) error {
	if p.isRunning {
		return ErrIsRunning
	}
	var ipv4 bool
	if isIPv4(ipaddr.IP) {
		ipv4 = true
	} else if isIPv6(ipaddr.IP) {
		ipv4 = false
	}

	p.ipaddr = ipaddr
	p.addr = ipaddr.String()
	p.ipv4 = ipv4
	p.sequence = 1

	return nil
}

// IPAddr returns the ip address of the target host.
func (p *Pinger) IPAddr() *net.IPAddr {
	return p.ipaddr
}

// SetAddr resolves and sets the ip address of the target host, addr can be a
// DNS name like "www.google.com" or IP like "127.0.0.1".
func (p *Pinger) SetAddr(addr string) error {
	if p.isRunning {
		return ErrIsRunning
	}
	ipaddr, err := net.ResolveIPAddr("ip", addr)
	if err != nil {
		return err
	}

	if err := p.SetIPAddr(ipaddr); err != nil {
		return err
	}
	p.addr = addr
	return nil
}

// Addr returns the string ip address of the target host.
func (p *Pinger) Addr() string {
	return p.addr
}

// SetPrivileged sets the type of ping pinger will send.
// false means pinger will send an "unprivileged" UDP ping.
// true means pinger will send a "privileged" raw ICMP ping.
// NOTE: setting to true requires that it be run with super-user privileges.
func (p *Pinger) SetPrivileged(privileged bool) error {
	if p.isRunning {
		return ErrIsRunning
	}
	if privileged {
		p.network = "ip"
	} else {
		p.network = "udp"
	}
	return nil
}

// Privileged returns whether pinger is running in privileged mode.
func (p *Pinger) Privileged() bool {
	return p.network == "ip"
}

// Run runs the pinger. This is a blocking function that will exit when it's
// done. If Count or Interval are not specified, it will run continuously until
// it is interrupted.
func (p *Pinger) Run(ctx context.Context, out chan<- Latency, errCh chan<- error) {
	var err error
	defer func() {
		if err != nil {
			errCh <- err
			close(errCh)
		}
	}()
	if p.isRunning {
		err = ErrIsRunning
		return
	}
	p.isRunning = true
	defer func() {
		p.isRunning = false
	}()

	var conn *icmp.PacketConn
	if p.ipv4 {
		if conn, err = p.listen(ipv4Proto[p.network]); err != nil {
			err = xerrors.Errorf("%v: %w", err, ErrConnIsNil)
			return
		}
		conn.IPv4PacketConn().SetControlMessage(ipv4.FlagTTL, true)
	} else {
		if conn, err = p.listen(ipv6Proto[p.network]); err != nil {
			err = xerrors.Errorf("%v: %w", err, ErrConnIsNil)
			return
		}

		conn.IPv6PacketConn().SetControlMessage(ipv6.FlagHopLimit, true)
	}
	defer conn.Close()

	recv := make(chan *packet, 5)
	defer close(recv)

	go p.recvICMP(ctx, conn, recv)

	interval := time.NewTicker(p.Interval)
	defer interval.Stop()

	for {
		select {
		case <-ctx.Done():
			err = ctx.Err()
			return
		case <-interval.C:
			sendCtx, cancel := context.WithTimeout(ctx, p.Timeout)
			if err = p.sendICMP(sendCtx, conn); err != nil {
				if xerrors.Is(err, context.DeadlineExceeded) {
					out <- Offline
				} else {
					err = xerrors.Errorf("failed to send ICMP: %w", err)
					return
				}
			}
			cancel() // release resources
		case r := <-recv:
			packet, err := p.processPacket(r)

			if err != nil && !(xerrors.Is(err, ErrNotAnEcho) ||
				xerrors.Is(err, ErrTrackersNotEqual) ||
				xerrors.Is(err, ErrReplyFromSameID)) {
				err = xerrors.Errorf("failed to recv icmp: %w", err)
				return
			}

			out <- Latency(packet.Rtt.Nanoseconds() / (1000 * 1000))
		}
	}
}

func (p *Pinger) recvICMP(
	ctx context.Context,
	conn *icmp.PacketConn,
	recv chan<- *packet,
) error {
	buf := make([]byte, 512)
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			conn.SetReadDeadline(time.Now().Add(time.Millisecond * 100))
			var n, ttl int
			var err error
			if p.ipv4 {
				var cm *ipv4.ControlMessage
				n, cm, _, err = conn.IPv4PacketConn().ReadFrom(buf)
				if cm != nil {
					ttl = cm.TTL
				}
			} else {
				var cm *ipv6.ControlMessage
				n, cm, _, err = conn.IPv6PacketConn().ReadFrom(buf)
				if cm != nil {
					ttl = cm.HopLimit
				}
			}
			if err != nil {
				if neterr, ok := err.(*net.OpError); ok {
					if neterr.Timeout() {
						// Read timeout
						continue
					} else {
						return err
					}
				}
			}

			recv <- &packet{bytes: buf, nbytes: n, ttl: ttl}
		}
	}
}

func (p *Pinger) processPacket(recv *packet) (*Packet, error) {
	receivedAt := time.Now()
	var bytes []byte
	var proto int
	if p.ipv4 {
		if p.network == "ip" {
			bytes = ipv4Payload(recv)
		} else {
			bytes = recv.bytes
		}
		proto = protocolICMP
	} else {
		bytes = recv.bytes
		proto = protocolIPv6ICMP
	}

	var m *icmp.Message
	var err error
	if m, err = icmp.ParseMessage(proto, bytes[:recv.nbytes]); err != nil {
		return nil, xerrors.Errorf("error parsing icmp message: %w", err)
	}

	if m.Type != ipv4.ICMPTypeEchoReply && m.Type != ipv6.ICMPTypeEchoReply {
		// Not an echo reply, ignore it
		return nil, ErrNotAnEcho
	}

	outPkt := &Packet{
		Nbytes: recv.nbytes,
		IPAddr: p.ipaddr,
		Addr:   p.addr,
		Ttl:    recv.ttl,
	}

	switch pkt := m.Body.(type) {
	case *icmp.Echo:
		// If we are priviledged, we can match icmp.ID
		if p.network == "ip" {
			// Check if reply from same ID
			if pkt.ID != p.id {
				return nil, ErrReplyFromSameID
			}
		}

		if len(pkt.Data) < timeSliceLength+trackerLength {
			return nil, xerrors.Errorf("insufficient data received; got: %d %v",
				len(pkt.Data), pkt.Data)
		}

		tracker := bytesToInt(pkt.Data[timeSliceLength:])
		timestamp := bytesToTime(pkt.Data[:timeSliceLength])

		if tracker != p.Tracker {
			return nil, ErrTrackersNotEqual
		}

		outPkt.Rtt = receivedAt.Sub(timestamp)
		outPkt.Seq = pkt.Seq
	default:
		// Very bad, not sure how this can happen
		return nil, xerrors.Errorf("invalid ICMP echo reply; type: '%T', '%v'", pkt, pkt)
	}

	return outPkt, nil
}

func (p *Pinger) sendICMP(ctx context.Context, conn *icmp.PacketConn) error {
	var typ icmp.Type
	if p.ipv4 {
		typ = ipv4.ICMPTypeEcho
	} else {
		typ = ipv6.ICMPTypeEchoRequest
	}

	var dst net.Addr = p.ipaddr
	if p.network == "udp" {
		dst = &net.UDPAddr{IP: p.ipaddr.IP, Zone: p.ipaddr.Zone}
	}

	t := append(timeToBytes(time.Now()), intToBytes(p.Tracker)...)
	if remainSize := p.Size - timeSliceLength - trackerLength; remainSize > 0 {
		t = append(t, bytes.Repeat([]byte{1}, remainSize)...)
	}

	body := &icmp.Echo{
		ID:   p.id,
		Seq:  p.sequence,
		Data: t,
	}

	msg := &icmp.Message{
		Type: typ,
		Code: 0,
		Body: body,
	}

	msgBytes, err := msg.Marshal(nil)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			break // break the select statement
		}

		if _, err := conn.WriteTo(msgBytes, dst); err != nil {
			if neterr, ok := err.(*net.OpError); ok {
				if neterr.Err == syscall.ENOBUFS {
					continue
				}
			}
		}
		p.sequence++
		break
	}

	return nil
}

func (p *Pinger) listen(netProto string) (*icmp.PacketConn, error) {
	conn, err := icmp.ListenPacket(netProto, p.Source)
	if err != nil {
		return nil, xerrors.Errorf("failed to listen for ICMP packets: %w", err)
	}
	return conn, nil
}

func ipv4Payload(recv *packet) []byte {
	b := recv.bytes
	if len(b) < ipv4.HeaderLen {
		return b
	}
	hdrlen := int(b[0]&0x0f) << 2
	recv.nbytes -= hdrlen
	return b[hdrlen:]
}

func bytesToTime(b []byte) time.Time {
	var nsec int64
	for i := uint8(0); i < 8; i++ {
		nsec += int64(b[i]) << ((7 - i) * 8)
	}
	return time.Unix(nsec/1000000000, nsec%1000000000)
}

func isIPv4(ip net.IP) bool {
	return len(ip.To4()) == net.IPv4len
}

func isIPv6(ip net.IP) bool {
	return len(ip) == net.IPv6len
}

func timeToBytes(t time.Time) []byte {
	nsec := t.UnixNano()
	b := make([]byte, 8)
	for i := uint8(0); i < 8; i++ {
		b[i] = byte((nsec >> ((7 - i) * 8)) & 0xff)
	}
	return b
}

func bytesToInt(b []byte) int64 {
	return int64(binary.BigEndian.Uint64(b))
}

func intToBytes(tracker int64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(tracker))
	return b
}
