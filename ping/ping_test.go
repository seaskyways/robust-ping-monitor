package ping

import (
	"context"
	"golang.org/x/xerrors"
	"testing"
	"time"
)

func TestNewPinger(t *testing.T) {
	pinger, _ := NewPinger()
	pinger.Timeout = time.Second * 1
	pinger.SetPrivileged(true)
	err := pinger.SetAddr("8.8.8.8")
	if err != nil {
		t.Errorf("failed to set address: %v", err)
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Second*10)
	out, errCh := make(chan Latency), make(chan error)
	go pinger.Run(ctx, out, errCh)

loop:
	for {
		select {
		case value, ok := <-out:
			if !ok {
				break loop
			} else {
				t.Logf("received ping: %v", value)

			}
		case err := <-errCh:
			if xerrors.Is(err, context.DeadlineExceeded) {
				t.Logf("success")
			} else {
				t.Errorf("failed: %v", err)
			}
			return
		}
	}
}
