package pingmonitor

import (
	"github.com/jinzhu/gorm"
	"log"
	"os"
	"path"
	"time"
)
import _ "github.com/jinzhu/gorm/dialects/sqlite"

const (
	LatencyOffline = -1
	LatencyUnknown = -2
	LatencyError   = -3
)

type pingEntry struct {
	IP       string `gorm:"primary_key;not null"`
	DeviceID int64  `gorm:"index:device_id"`
	IsIPv6   bool   `gorm:"index:ipv6"`
	Latency  int16

	TimeUpdated time.Time `gorm:""`
}

type repository struct {
	db *gorm.DB
}

func newRepository() *repository {
	var err error
	pm := &repository{}
	dir, _ := os.Getwd()
	pm.db, err = gorm.Open("sqlite3", path.Join(dir, "db_monitor.sqlite"))
	if err != nil {
		log.Fatalf("failed to open db: %v", err)
	}

	// Create tables if not exists
	pm.db.AutoMigrate(&pingEntry{})

	return pm
}

func (pm *repository) saveIP(deviceID int64, ip string, latency int16, isIPv6 bool) error {
	return pm.db.Save(&pingEntry{
		DeviceID:    deviceID,
		IP:          ip,
		Latency:     latency,
		IsIPv6:      isIPv6,
		TimeUpdated: time.Now(),
	}).Error
}

func (pm *repository) updateIPLatency(ip string, latency int16) error {
	return pm.db.Save(&pingEntry{
		IP:          ip,
		Latency:     latency,
		TimeUpdated: time.Now(),
	}).Error
}

func (pm *repository) deleteDevice(deviceID int64) error {
	return pm.db.Delete(&pingEntry{}, pingEntry{DeviceID: deviceID}).Error
}

func (pm *repository) getDevice(deviceID int64) (out []pingEntry, err error) {
	err = pm.db.Find(&out, pingEntry{DeviceID: deviceID}).Error
	return
}

func (pm *repository) getIP(ip string) (out *pingEntry, err error) {
	err = pm.db.First(&out, pingEntry{IP: ip}).Error
	return
}
