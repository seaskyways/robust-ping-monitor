package pingmonitor

import (
	"context"
	"fmt"
	"golang.org/x/xerrors"
	"robust-ping-monitor/ping"
)

func (rs *rpcServer) monitor() {
	cancelMap := map[int64][]context.CancelFunc{}

	for { // keep looping
		select {
		case id := <-rs.watchDeviceIDCh:
			entries, _ := rs.repo.getDevice(id)
			for _, entry := range entries {
				ctx, cancel := context.WithCancel(context.Background())
				cancelMap[id] = append(cancelMap[id], cancel)
				go rs.launchPinger(ctx, entry.IP)
			}

		case id := <-rs.stopDeviceIDCh:
			for _, cancelFunc := range cancelMap[id] {
				cancelFunc()
			}
		}
	}
}

func (rs *rpcServer) launchPinger(ctx context.Context, ipAddr string) {
	pinger := rs.pingerPool.Get().(*ping.Pinger)
	defer rs.pingerPool.Put(pinger)

	_ = pinger.SetAddr(ipAddr)
	outCh, errCh := make(chan ping.Latency), make(chan error)
	pinger.Run(ctx, outCh, errCh)

	for {
		select {
		case err := <-errCh:
			if xerrors.Is(err, context.Canceled) {
				return
			}

			_ = rs.repo.updateIPLatency(ipAddr, LatencyError)
			fmt.Printf("failed to update IP latency: %v", err)
			close(outCh)
			close(errCh)
			return
		case l := <-outCh:
			_ = rs.repo.updateIPLatency(ipAddr, int16(l))
		}
	}
}
