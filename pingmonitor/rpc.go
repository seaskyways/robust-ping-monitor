package pingmonitor

import (
	"context"
	"golang.org/x/xerrors"
	"robust-ping-monitor/ping"
	"robust-ping-monitor/proto"
	"sync"
	"sync/atomic"
)

var ErrDeviceNotFound = xerrors.New("device not found")

type rpcServer struct {
	pingerPool sync.Pool
	repo       *repository

	// this channel is for all clients who are listening
	// for changes for latency for devices
	clientWatchedDevices map[int64]map[uint64]chan int16
	watchCounter         uint64

	watchDeviceIDCh chan int64
	stopDeviceIDCh  chan int64
}

func NewRPCServer() monitor.MonitorServer {
	repo := newRepository()
	server := &rpcServer{
		repo: repo,
		pingerPool: sync.Pool{
			New: func() interface{} {
				pinger, _ := ping.NewPinger()
				return pinger
			},
		},
		watchDeviceIDCh: make(chan int64, 16),
		stopDeviceIDCh:  make(chan int64, 16),
	}
	go server.monitor()
	return server
}

func (rs *rpcServer) GetDevicePing(ctx context.Context, id *monitor.DeviceID) (*monitor.DevicePing, error) {
	pingEntries, err := rs.repo.getDevice(id.ID)
	if err != nil || len(pingEntries) == 0 {
		return nil, ErrDeviceNotFound
	}
	out := &monitor.DevicePing{ID: id.ID, IPv4Pings: map[string]*monitor.Ping{}, IPv6Pings: map[string]*monitor.Ping{}}
	for _, entry := range pingEntries {
		//region: check if request is being cancelled
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
		}
		//endregion

		outPing := &monitor.Ping{
			IP:           entry.IP,
			Latency:      int32(entry.Latency),
			PingDateUnix: entry.TimeUpdated.Unix(),
		}
		if entry.IsIPv6 {
			out.IPv6Pings[entry.IP] = outPing
		} else {
			out.IPv4Pings[entry.IP] = outPing
		}
	}
	return out, nil
}

func (rs *rpcServer) DeleteDevice(ctx context.Context, id *monitor.DeviceID) (*monitor.DeviceID, error) {
	defer func() {
		if chs, ok := rs.clientWatchedDevices[id.ID]; ok {
			for _, ch := range chs {
				close(ch)
			}
			delete(rs.clientWatchedDevices, id.ID)
		}
	}()

	if err := rs.repo.deleteDevice(id.ID); err != nil {
		return nil, err
	} else {
		return id, nil
	}
}

func (rs *rpcServer) SaveDevice(ctx context.Context, d *monitor.Device) (*monitor.DeviceID, error) {
	for _, ip := range d.IPv4 {
		if err := rs.repo.saveIP(d.ID, ip, LatencyUnknown, false); err != nil {
			return nil, err
		}
	}
	for _, ip := range d.IPv6 {
		if err := rs.repo.saveIP(d.ID, ip, LatencyUnknown, true); err != nil {
			return nil, err
		}
	}

	return &monitor.DeviceID{ID: d.ID}, nil
}

func (rs *rpcServer) WatchDevice(id *monitor.DeviceID, srv monitor.Monitor_WatchDeviceServer) error {
	if _, ok := rs.clientWatchedDevices[id.ID]; !ok {
		return ErrDeviceNotFound
	}

	watcherID := atomic.AddUint64(&rs.watchCounter, 1)
	updatesCh := make(chan int16)

	rs.clientWatchedDevices[id.ID][watcherID] = updatesCh
	defer func() {
		delete(rs.clientWatchedDevices[id.ID], watcherID)
	}()

	ctx := srv.Context()
	for {
		select {
		case <-ctx.Done():
			return nil
		case _, ok := <-updatesCh:
			if !ok {
				return nil
			}

			devicePing := &monitor.DevicePing{
				ID:        id.ID,
				IPv4Pings: make(map[string]*monitor.Ping),
				IPv6Pings: make(map[string]*monitor.Ping),
			}
			entries, _ := rs.repo.getDevice(id.ID)
			for _, entry := range entries {
				mPing := &monitor.Ping{
					IP:           entry.IP,
					Latency:      int32(entry.Latency),
					PingDateUnix: entry.TimeUpdated.Unix(),
				}
				if entry.IsIPv6 {
					devicePing.IPv6Pings[entry.IP] = mPing
				} else {
					devicePing.IPv4Pings[entry.IP] = mPing
				}
			}
			if err := srv.Send(devicePing); err != nil {
				return err
			}
		}
	}
}
