module robust-ping-monitor

go 1.12

require (
	github.com/digineo/go-ping v1.0.0
	github.com/golang/protobuf v1.3.1
	github.com/jinzhu/gorm v1.9.8
	github.com/sparrc/go-ping v0.0.0-20190604181555-e33cfb8ae7ed
	golang.org/x/net v0.0.0-20190311183353-d8887717615a
	golang.org/x/xerrors v0.0.0-20190513163551-3ee3066db522
	google.golang.org/grpc v1.21.1
)
