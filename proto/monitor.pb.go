// Code generated by protoc-gen-go. DO NOT EDIT.
// source: monitor.proto

package monitor

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Device struct {
	ID                   int64    `protobuf:"varint,1,opt,name=ID,json=iD,proto3" json:"ID,omitempty"`
	IPv4                 []string `protobuf:"bytes,2,rep,name=IPv4,json=iPv4,proto3" json:"IPv4,omitempty"`
	IPv6                 []string `protobuf:"bytes,3,rep,name=IPv6,json=iPv6,proto3" json:"IPv6,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Device) Reset()         { *m = Device{} }
func (m *Device) String() string { return proto.CompactTextString(m) }
func (*Device) ProtoMessage()    {}
func (*Device) Descriptor() ([]byte, []int) {
	return fileDescriptor_44174b7b2a306b71, []int{0}
}

func (m *Device) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Device.Unmarshal(m, b)
}
func (m *Device) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Device.Marshal(b, m, deterministic)
}
func (m *Device) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Device.Merge(m, src)
}
func (m *Device) XXX_Size() int {
	return xxx_messageInfo_Device.Size(m)
}
func (m *Device) XXX_DiscardUnknown() {
	xxx_messageInfo_Device.DiscardUnknown(m)
}

var xxx_messageInfo_Device proto.InternalMessageInfo

func (m *Device) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

func (m *Device) GetIPv4() []string {
	if m != nil {
		return m.IPv4
	}
	return nil
}

func (m *Device) GetIPv6() []string {
	if m != nil {
		return m.IPv6
	}
	return nil
}

type DeviceID struct {
	ID                   int64    `protobuf:"varint,1,opt,name=ID,json=iD,proto3" json:"ID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeviceID) Reset()         { *m = DeviceID{} }
func (m *DeviceID) String() string { return proto.CompactTextString(m) }
func (*DeviceID) ProtoMessage()    {}
func (*DeviceID) Descriptor() ([]byte, []int) {
	return fileDescriptor_44174b7b2a306b71, []int{1}
}

func (m *DeviceID) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeviceID.Unmarshal(m, b)
}
func (m *DeviceID) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeviceID.Marshal(b, m, deterministic)
}
func (m *DeviceID) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeviceID.Merge(m, src)
}
func (m *DeviceID) XXX_Size() int {
	return xxx_messageInfo_DeviceID.Size(m)
}
func (m *DeviceID) XXX_DiscardUnknown() {
	xxx_messageInfo_DeviceID.DiscardUnknown(m)
}

var xxx_messageInfo_DeviceID proto.InternalMessageInfo

func (m *DeviceID) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

type Ping struct {
	IP                   string   `protobuf:"bytes,1,opt,name=IP,json=iP,proto3" json:"IP,omitempty"`
	Latency              int32    `protobuf:"varint,2,opt,name=Latency,json=latency,proto3" json:"Latency,omitempty"`
	PingDateUnix         int64    `protobuf:"varint,3,opt,name=PingDateUnix,json=pingDateUnix,proto3" json:"PingDateUnix,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Ping) Reset()         { *m = Ping{} }
func (m *Ping) String() string { return proto.CompactTextString(m) }
func (*Ping) ProtoMessage()    {}
func (*Ping) Descriptor() ([]byte, []int) {
	return fileDescriptor_44174b7b2a306b71, []int{2}
}

func (m *Ping) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Ping.Unmarshal(m, b)
}
func (m *Ping) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Ping.Marshal(b, m, deterministic)
}
func (m *Ping) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Ping.Merge(m, src)
}
func (m *Ping) XXX_Size() int {
	return xxx_messageInfo_Ping.Size(m)
}
func (m *Ping) XXX_DiscardUnknown() {
	xxx_messageInfo_Ping.DiscardUnknown(m)
}

var xxx_messageInfo_Ping proto.InternalMessageInfo

func (m *Ping) GetIP() string {
	if m != nil {
		return m.IP
	}
	return ""
}

func (m *Ping) GetLatency() int32 {
	if m != nil {
		return m.Latency
	}
	return 0
}

func (m *Ping) GetPingDateUnix() int64 {
	if m != nil {
		return m.PingDateUnix
	}
	return 0
}

type DevicePing struct {
	ID                   int64            `protobuf:"varint,1,opt,name=ID,json=iD,proto3" json:"ID,omitempty"`
	IPv4Pings            map[string]*Ping `protobuf:"bytes,2,rep,name=IPv4Pings,json=iPv4Pings,proto3" json:"IPv4Pings,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	IPv6Pings            map[string]*Ping `protobuf:"bytes,3,rep,name=IPv6Pings,json=iPv6Pings,proto3" json:"IPv6Pings,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *DevicePing) Reset()         { *m = DevicePing{} }
func (m *DevicePing) String() string { return proto.CompactTextString(m) }
func (*DevicePing) ProtoMessage()    {}
func (*DevicePing) Descriptor() ([]byte, []int) {
	return fileDescriptor_44174b7b2a306b71, []int{3}
}

func (m *DevicePing) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DevicePing.Unmarshal(m, b)
}
func (m *DevicePing) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DevicePing.Marshal(b, m, deterministic)
}
func (m *DevicePing) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DevicePing.Merge(m, src)
}
func (m *DevicePing) XXX_Size() int {
	return xxx_messageInfo_DevicePing.Size(m)
}
func (m *DevicePing) XXX_DiscardUnknown() {
	xxx_messageInfo_DevicePing.DiscardUnknown(m)
}

var xxx_messageInfo_DevicePing proto.InternalMessageInfo

func (m *DevicePing) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

func (m *DevicePing) GetIPv4Pings() map[string]*Ping {
	if m != nil {
		return m.IPv4Pings
	}
	return nil
}

func (m *DevicePing) GetIPv6Pings() map[string]*Ping {
	if m != nil {
		return m.IPv6Pings
	}
	return nil
}

func init() {
	proto.RegisterType((*Device)(nil), "Device")
	proto.RegisterType((*DeviceID)(nil), "DeviceID")
	proto.RegisterType((*Ping)(nil), "Ping")
	proto.RegisterType((*DevicePing)(nil), "DevicePing")
	proto.RegisterMapType((map[string]*Ping)(nil), "DevicePing.IPv4PingsEntry")
	proto.RegisterMapType((map[string]*Ping)(nil), "DevicePing.IPv6PingsEntry")
}

func init() { proto.RegisterFile("monitor.proto", fileDescriptor_44174b7b2a306b71) }

var fileDescriptor_44174b7b2a306b71 = []byte{
	// 329 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xa4, 0x92, 0xc1, 0x4b, 0xc3, 0x30,
	0x14, 0xc6, 0x69, 0xb2, 0xad, 0xf6, 0x75, 0x1b, 0x92, 0x53, 0xa8, 0x97, 0x52, 0x04, 0x77, 0x2a,
	0x32, 0x47, 0x19, 0x9e, 0x04, 0x23, 0x32, 0x50, 0x28, 0x51, 0xf1, 0x5c, 0x47, 0xd0, 0xe0, 0x6c,
	0xc7, 0x8c, 0xc5, 0xfd, 0x2b, 0x5e, 0xfd, 0x47, 0xa5, 0x49, 0xd6, 0x65, 0xd3, 0x9b, 0xb7, 0xbe,
	0xef, 0xbd, 0xef, 0xd7, 0xf7, 0xf5, 0x15, 0x06, 0x6f, 0x55, 0x29, 0x55, 0xb5, 0x4a, 0x97, 0xab,
	0x4a, 0x55, 0xc9, 0x05, 0xf4, 0x98, 0xa8, 0xe5, 0x5c, 0x90, 0x21, 0xa0, 0x19, 0xa3, 0x5e, 0xec,
	0x8d, 0x30, 0x47, 0x92, 0x11, 0x02, 0x9d, 0x59, 0x5e, 0x4f, 0x28, 0x8a, 0xf1, 0x28, 0xe0, 0x1d,
	0x99, 0xd7, 0x13, 0xab, 0x65, 0x14, 0xb7, 0x5a, 0x96, 0x44, 0x70, 0x60, 0x08, 0x33, 0xb6, 0xcf,
	0x48, 0xee, 0xa1, 0x93, 0xcb, 0xf2, 0x59, 0xeb, 0xb9, 0xd6, 0x03, 0x8e, 0x64, 0x4e, 0x28, 0xf8,
	0x37, 0x85, 0x12, 0xe5, 0x7c, 0x4d, 0x51, 0xec, 0x8d, 0xba, 0xdc, 0x5f, 0x98, 0x92, 0x24, 0xd0,
	0x6f, 0x1c, 0xac, 0x50, 0xe2, 0xa1, 0x94, 0x9f, 0x14, 0x6b, 0x56, 0x7f, 0xe9, 0x68, 0xc9, 0x17,
	0x02, 0x30, 0xaf, 0x6c, 0xe1, 0xee, 0xe2, 0x53, 0x08, 0x9a, 0xc5, 0x9b, 0xde, 0xbb, 0xde, 0x3e,
	0x1c, 0x47, 0xe9, 0x76, 0x3e, 0x6d, 0x9b, 0x57, 0xa5, 0x5a, 0xad, 0x79, 0x20, 0x37, 0xb5, 0x75,
	0x66, 0xc6, 0x89, 0xff, 0x74, 0x66, 0x7b, 0x4e, 0x53, 0x47, 0x97, 0x30, 0xdc, 0xc5, 0x92, 0x43,
	0xc0, 0xaf, 0x62, 0x6d, 0x33, 0x37, 0x8f, 0xe4, 0x08, 0xba, 0x75, 0xb1, 0xf8, 0x10, 0x3a, 0x72,
	0x38, 0xee, 0xa6, 0xcd, 0x34, 0x37, 0xda, 0x39, 0x9a, 0x7a, 0x16, 0x92, 0xfd, 0x0b, 0x32, 0xfe,
	0xf6, 0xc0, 0xbf, 0x35, 0x27, 0x26, 0x31, 0xc0, 0x5d, 0x51, 0x0b, 0x7b, 0x60, 0xdf, 0x46, 0x89,
	0x82, 0xb4, 0x3d, 0xd8, 0x09, 0x0c, 0xae, 0x85, 0x72, 0x3e, 0xe6, 0xb6, 0x17, 0x85, 0x4e, 0x74,
	0x72, 0x0c, 0x7d, 0x26, 0x16, 0x42, 0x6d, 0x60, 0xce, 0xdc, 0x0e, 0x2e, 0x7c, 0x2c, 0xd4, 0xfc,
	0xe5, 0xf7, 0x90, 0x0b, 0x3b, 0xf5, 0x9e, 0x7a, 0xfa, 0xef, 0x3b, 0xfb, 0x09, 0x00, 0x00, 0xff,
	0xff, 0xb0, 0x7b, 0x1d, 0x89, 0x8e, 0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// MonitorClient is the client API for Monitor service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type MonitorClient interface {
	SaveDevice(ctx context.Context, in *Device, opts ...grpc.CallOption) (*DeviceID, error)
	GetDevicePing(ctx context.Context, in *DeviceID, opts ...grpc.CallOption) (*DevicePing, error)
	DeleteDevice(ctx context.Context, in *DeviceID, opts ...grpc.CallOption) (*DeviceID, error)
	WatchDevice(ctx context.Context, in *DeviceID, opts ...grpc.CallOption) (Monitor_WatchDeviceClient, error)
}

type monitorClient struct {
	cc *grpc.ClientConn
}

func NewMonitorClient(cc *grpc.ClientConn) MonitorClient {
	return &monitorClient{cc}
}

func (c *monitorClient) SaveDevice(ctx context.Context, in *Device, opts ...grpc.CallOption) (*DeviceID, error) {
	out := new(DeviceID)
	err := c.cc.Invoke(ctx, "/Monitor/SaveDevice", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monitorClient) GetDevicePing(ctx context.Context, in *DeviceID, opts ...grpc.CallOption) (*DevicePing, error) {
	out := new(DevicePing)
	err := c.cc.Invoke(ctx, "/Monitor/GetDevicePing", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monitorClient) DeleteDevice(ctx context.Context, in *DeviceID, opts ...grpc.CallOption) (*DeviceID, error) {
	out := new(DeviceID)
	err := c.cc.Invoke(ctx, "/Monitor/DeleteDevice", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *monitorClient) WatchDevice(ctx context.Context, in *DeviceID, opts ...grpc.CallOption) (Monitor_WatchDeviceClient, error) {
	stream, err := c.cc.NewStream(ctx, &_Monitor_serviceDesc.Streams[0], "/Monitor/WatchDevice", opts...)
	if err != nil {
		return nil, err
	}
	x := &monitorWatchDeviceClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Monitor_WatchDeviceClient interface {
	Recv() (*DevicePing, error)
	grpc.ClientStream
}

type monitorWatchDeviceClient struct {
	grpc.ClientStream
}

func (x *monitorWatchDeviceClient) Recv() (*DevicePing, error) {
	m := new(DevicePing)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// MonitorServer is the server API for Monitor service.
type MonitorServer interface {
	SaveDevice(context.Context, *Device) (*DeviceID, error)
	GetDevicePing(context.Context, *DeviceID) (*DevicePing, error)
	DeleteDevice(context.Context, *DeviceID) (*DeviceID, error)
	WatchDevice(*DeviceID, Monitor_WatchDeviceServer) error
}

func RegisterMonitorServer(s *grpc.Server, srv MonitorServer) {
	s.RegisterService(&_Monitor_serviceDesc, srv)
}

func _Monitor_SaveDevice_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Device)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonitorServer).SaveDevice(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Monitor/SaveDevice",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonitorServer).SaveDevice(ctx, req.(*Device))
	}
	return interceptor(ctx, in, info, handler)
}

func _Monitor_GetDevicePing_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeviceID)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonitorServer).GetDevicePing(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Monitor/GetDevicePing",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonitorServer).GetDevicePing(ctx, req.(*DeviceID))
	}
	return interceptor(ctx, in, info, handler)
}

func _Monitor_DeleteDevice_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeviceID)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MonitorServer).DeleteDevice(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Monitor/DeleteDevice",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MonitorServer).DeleteDevice(ctx, req.(*DeviceID))
	}
	return interceptor(ctx, in, info, handler)
}

func _Monitor_WatchDevice_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(DeviceID)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(MonitorServer).WatchDevice(m, &monitorWatchDeviceServer{stream})
}

type Monitor_WatchDeviceServer interface {
	Send(*DevicePing) error
	grpc.ServerStream
}

type monitorWatchDeviceServer struct {
	grpc.ServerStream
}

func (x *monitorWatchDeviceServer) Send(m *DevicePing) error {
	return x.ServerStream.SendMsg(m)
}

var _Monitor_serviceDesc = grpc.ServiceDesc{
	ServiceName: "Monitor",
	HandlerType: (*MonitorServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SaveDevice",
			Handler:    _Monitor_SaveDevice_Handler,
		},
		{
			MethodName: "GetDevicePing",
			Handler:    _Monitor_GetDevicePing_Handler,
		},
		{
			MethodName: "DeleteDevice",
			Handler:    _Monitor_DeleteDevice_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "WatchDevice",
			Handler:       _Monitor_WatchDevice_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "monitor.proto",
}
