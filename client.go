package main

import (
	"context"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"os"
	"robust-ping-monitor/proto"
	"strings"
)

var socketPath string

const usage = `
	rmon --save --dID 1 --ipV4 192.168.0.1,1.2.3.4,2.4.6.7 --ipV6 ffe0::beef,ffae::feeb
	
	rmon --get --dID 1 
		
	rmon --del --dID 1
	
	rmon --watch --dID 1

Notes:
1- A device can be given multiple IPs. Calling "save" multiple times does NOT replace old IPs.

2- This is not meant for programmatic use. If you need to use Robust Monitor in your program,
use the proto files included in the source code.

3- Env variable PM_SOCKET is used to locate the Unix domain socket of Robust Monitor.
`

func main() {
	socketPath = os.Getenv("PM_SOCKET")
	conn, err := grpc.Dial("unix://"+socketPath, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("couldn't connect to socket: %v", err)
	}
	client := monitor.NewMonitorClient(conn)

	var deviceId int64
	var ipV4, ipV6 string
	var save, get, del, watch bool

	flag.Int64Var(&deviceId, "dID", 0, "device ID")
	flag.StringVar(&ipV4, "ipV4", "", "list of IPv4 IPs to be monitored")
	flag.StringVar(&ipV6, "ipV6", "", "list of IPv6 IPs to be monitored")
	flag.BoolVar(&save, "save", false, "save")
	flag.BoolVar(&get, "get", false, "get")
	flag.BoolVar(&del, "del", false, "del")
	flag.BoolVar(&watch, "watch", false, "watch")

	flag.Parse()

	if deviceId == 0 {
		flag.Usage()
		fmt.Println(usage)
		return
	}

	ctx := context.Background()

actions:
	switch {
	case save:
		if len(ipV4) == 0 && len(ipV6) == 0 {
			flag.Usage()
			fmt.Println(usage)
			return
		}
		var v4Ips []string
		var v6Ips []string
		if len(ipV4) > 0 {
			v4Ips = strings.Split(ipV4, ",")
		}
		if len(ipV6) > 0 {
			v6Ips = strings.Split(ipV6, ",")
		}
		id, err := client.SaveDevice(ctx, &monitor.Device{
			ID:   deviceId,
			IPv4: v4Ips,
			IPv6: v6Ips,
		})
		if err != nil {
			fmt.Printf("error: %v", err)
			os.Exit(1)
		} else {
			fmt.Printf("device saved successfully, id=%d", id)
		}
	case get:
		ping, err := client.GetDevicePing(ctx, &monitor.DeviceID{ID: deviceId})
		if err != nil {
			fmt.Printf("error: %v", err)
			os.Exit(1)
		} else {
			printPing(ping)
		}
	case del:
		id, err := client.DeleteDevice(ctx, &monitor.DeviceID{ID: deviceId})
		if err != nil {
			fmt.Printf("error: %v", err)
			os.Exit(1)
		}
		fmt.Printf("successfully deleted Device %d", id)
	case watch:
		watch, err := client.WatchDevice(ctx, &monitor.DeviceID{ID: deviceId})
		if err != nil {
			fmt.Printf("error: %v", err)
			os.Exit(1)
		}
		for {
			ping, err := watch.Recv()
			if err != nil {
				break actions
			}
			printPing(ping)
			fmt.Println()
		}
	default:
		flag.Usage()
		fmt.Println(usage)
		return
	}

	fmt.Println()
}

func printPing(ping *monitor.DevicePing) {
	fmt.Printf("Device ID: %d\n", ping.ID)
	if len(ping.IPv4Pings) > 0 {
		fmt.Println("	IPv4:")
		for _, ping := range ping.IPv4Pings {
			var onlineMsg string
			if ping.Latency >= 0 {
				onlineMsg = "Online"
			} else {
				onlineMsg = "Unreachable"
			}
			fmt.Printf("		%s: %s\n", ping.IP, onlineMsg)
			fmt.Printf("		Latency: %d\n", ping.Latency)
		}
	}
	if len(ping.IPv6Pings) > 0 {
		fmt.Println("	IPv6:")
		for _, ping := range ping.IPv6Pings {
			var onlineMsg string
			if ping.Latency >= 0 {
				onlineMsg = "Online"
			} else {
				onlineMsg = "Unreachable"
			}
			fmt.Printf("		%s: %s\n", ping.IP, onlineMsg)
			fmt.Printf("		Latency: %d=\n", ping.Latency)
		}
	}
}
